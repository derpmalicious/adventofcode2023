use std::fs;

#[derive(Debug)]
enum Color {
	Red,
	Green,
	Blue,
}

impl Color {
	pub fn from_str(color: &str) -> Self {
		match color {
			"red" => Self::Red,
			"green" => Self::Green,
			"blue" => Self::Blue,
			_ => panic!("Invalid color {color}"),
		}
	}

	pub const fn max(&self) -> u32 {
		match self {
			Self::Red => 12,
			Self::Green => 13,
			Self::Blue => 14,
		}
	}
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day2.txt").unwrap();

	let mut possible_sum = 0;

	for line in input.lines() {
		let (game, sets) = line.split_once(": ").unwrap();
		let game_id: u32 = game.split_once(' ').unwrap().1.parse().unwrap();
		let mut impossible = false;

		for set in sets.split(';') {
			let set = set.trim();
			for cubes in set.split(", ") {
				let (count, color) = cubes.split_once(' ').unwrap();
				let count: u32 = count.parse().unwrap();
				let color = Color::from_str(color);

				if count > color.max() {
					impossible = true;
				}
			}
		}

		if !impossible {
			possible_sum += game_id;
		}
	}

	println!("{possible_sum}");
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day2.txt").unwrap();

	let mut power = 0;

	for line in input.lines() {
		let (_, sets) = line.split_once(": ").unwrap();

		let mut max_r = 0;
		let mut max_g = 0;
		let mut max_b = 0;

		for set in sets.split(';') {
			let set = set.trim();
			for cubes in set.split(", ") {
				let (count, color) = cubes.split_once(' ').unwrap();
				let count: u32 = count.parse().unwrap();
				let color = Color::from_str(color);

				match color {
					Color::Red => max_r = u32::max(max_r, count),
					Color::Green => max_g = u32::max(max_g, count),
					Color::Blue => max_b = u32::max(max_b, count),
				}
			}
		}

		power += max_r * max_g * max_b;
	}

	println!("{power}");
}
