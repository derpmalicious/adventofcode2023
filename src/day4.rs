use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day4.txt").unwrap();

	let mut total_score = 0;

	for line in input.lines() {
		let (_, numbers_str) = line.split_once(": ").unwrap();

		let (winning_str, have_str) = numbers_str.split_once(" | ").unwrap();

		let winning_nrs: Vec<u32> =
			winning_str.split_whitespace().map(|v| v.parse().unwrap()).collect();
		let have_nrs: Vec<u32> = have_str.split_whitespace().map(|v| v.parse().unwrap()).collect();

		let mut card_score = 0;
		for have_nr in have_nrs {
			if winning_nrs.contains(&have_nr) {
				if card_score == 0 {
					card_score = 1;
				} else {
					card_score *= 2;
				}
			}
		}

		total_score += card_score;
	}

	println!("{total_score}");
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day4.txt").unwrap();

	let mut cards = Vec::new();

	// Calculate scores and win counts of each card
	for line in input.lines() {
		// Let's assume cards are in order
		let (_, numbers_str) = line.split_once(": ").unwrap();

		let (winning_str, have_str) = numbers_str.split_once(" | ").unwrap();

		let winning_nrs: Vec<u32> =
			winning_str.split_whitespace().map(|v| v.parse().unwrap()).collect();
		let have_nrs: Vec<u32> = have_str.split_whitespace().map(|v| v.parse().unwrap()).collect();

		let mut card_wins = 0;
		for have_nr in have_nrs {
			if winning_nrs.contains(&have_nr) {
				card_wins += 1;
			}
		}

		cards.push(card_wins);
	}

	let mut total_score = 0;

	for (card_n, _) in cards.iter().enumerate() {
		total_score += calc_cards(&cards, card_n as u32);
	}

	println!("{total_score}");
}

pub fn calc_cards(cards: &Vec<u32>, card: u32) -> u32 {
	let mut total_cards = 1;
	for card_n in card + 1..card + 1 + cards[card as usize] {
		total_cards += calc_cards(cards, card_n);
	}
	total_cards
}
