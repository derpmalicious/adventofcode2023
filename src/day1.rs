use std::fs;

pub fn pt1() {
	let input = fs::read_to_string("inputs/day1.txt").unwrap();

	let mut total = 0;
	for line in input.lines() {
		let c1 = line.chars().find(char::is_ascii_digit).unwrap();
		let c2 = line.chars().rev().find(char::is_ascii_digit).unwrap();

		total += format!("{c1}{c2}").parse::<u32>().unwrap();
	}

	println!("{total}");
}

// Wtf is this. This is supposed to be day 1 pt2
pub fn pt2() {
	let input = fs::read_to_string("inputs/day1.txt").unwrap();
	let total = input.lines().fold(0, |acc, line| {
		let filter = |line: &str| {
			let first_char = line.chars().next().unwrap();
			match first_char {
				'0'..='9' => return first_char.to_digit(10).unwrap(),
				_ => 0,
			};

			match line {
				_ if line.starts_with("one") => 1,
				_ if line.starts_with("two") => 2,
				_ if line.starts_with("three") => 3,
				_ if line.starts_with("four") => 4,
				_ if line.starts_with("five") => 5,
				_ if line.starts_with("six") => 6,
				_ if line.starts_with("seven") => 7,
				_ if line.starts_with("eight") => 8,
				_ if line.starts_with("nine") => 9,
				_ => 0,
			}
		};

		let mut it_line = line;
		let mut r1 = 0;
		while !it_line.is_empty() {
			let result = filter(it_line);

			if result != 0 {
				r1 = result;
				break;
			}

			it_line = &it_line[1..];
		}

		let mut r2 = r1;
		for n in 1..line.len() {
			let line = &line[line.len() - n..];
			let result = filter(line);

			if result != 0 {
				r2 = result;
				break;
			}
		}

		acc + format!("{r1}{r2}").parse::<u32>().unwrap()
	});

	println!("{total}");
}
