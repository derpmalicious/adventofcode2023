use std::{cell::RefCell, fs, iter::repeat, rc::Rc};

#[derive(Debug, Clone)]
enum GridCell {
	Empty,
	Symbol(char),
	Number(Rc<RefCell<u32>>),
}

pub fn pt1() {
	let input = fs::read_to_string("inputs/day3.txt").unwrap();

	let mut grid: Vec<Vec<GridCell>> = Vec::new();

	// Construct the grid
	for mut line in input.lines() {
		let mut constructed_line = Vec::new();

		loop {
			if line.is_empty() {
				break;
			}

			let c = line.chars().next().unwrap();

			if char::is_ascii_digit(&c) {
				let num_n;
				let num: u32 = if let Some(num_end) = line.find(|c| !char::is_ascii_digit(&c)) {
					let l = &line[..num_end];
					num_n = num_end;
					line = &line[num_end..];
					l
				} else {
					let l = line;
					num_n = line.len();
					line = &line[line.len()..];
					l
				}
				.parse()
				.unwrap();

				constructed_line.append(
					&mut repeat(GridCell::Number(Rc::new(RefCell::new(num))))
						.take(num_n)
						.collect::<Vec<GridCell>>(),
				);
			} else if c == '.' {
				constructed_line.push(GridCell::Empty);
				line = &line[1..];
			} else {
				constructed_line.push(GridCell::Symbol(c));
				line = &line[1..];
			}
		}

		grid.push(constructed_line);
	}

	let mut parts_total = 0;

	// Check the grid
	for (row, line) in grid.iter().enumerate() {
		for (col, value) in line.iter().enumerate() {
			// Symbol. Check adjacent
			if let GridCell::Symbol(_) = value {
				// Consider what cells to check
				let (start_row, rows) = if row == 0 {
					(0, 2)
				} else if row == grid.len() - 1 {
					(row - 1, 2)
				} else {
					(row - 1, 3)
				};

				let (start_col, cols) = if col == 0 {
					(0, 2)
				} else if col == line.len() - 1 {
					(col - 1, 2)
				} else {
					(col - 1, 3)
				};

				// Check the cells
				for row in grid.iter().skip(start_row).take(rows) {
					for cell in row.iter().skip(start_col).take(cols) {
						if let GridCell::Number(n) = &cell {
							parts_total += *n.borrow();
							// Don't add the same number twice
							n.replace(0);
						}
					}
				}
			}
		}
	}

	println!("{parts_total}");
}

pub fn pt2() {
	let input = fs::read_to_string("inputs/day3.txt").unwrap();

	let mut grid: Vec<Vec<GridCell>> = Vec::new();

	// Construct the grid
	for mut line in input.lines() {
		let mut constructed_line = Vec::new();

		loop {
			if line.is_empty() {
				break;
			}

			let c = line.chars().next().unwrap();

			if char::is_ascii_digit(&c) {
				let num_n;
				let num: u32 = if let Some(num_end) = line.find(|c| !char::is_ascii_digit(&c)) {
					let l = &line[..num_end];
					num_n = num_end;
					line = &line[num_end..];
					l
				} else {
					let l = line;
					num_n = line.len();
					line = &line[line.len()..];
					l
				}
				.parse()
				.unwrap();

				constructed_line.append(
					&mut repeat(GridCell::Number(Rc::new(RefCell::new(num))))
						.take(num_n)
						.collect::<Vec<GridCell>>(),
				);
			} else if c == '.' {
				constructed_line.push(GridCell::Empty);
				line = &line[1..];
			} else {
				constructed_line.push(GridCell::Symbol(c));
				line = &line[1..];
			}
		}

		grid.push(constructed_line);
	}

	let mut parts_total = 0;

	// Check the grid
	for (row, line) in grid.iter().enumerate() {
		for (col, value) in line.iter().enumerate() {
			// Symbol. Check adjacent
			if let GridCell::Symbol(sym) = value {
				if *sym == '*' {
					// Consider what cells to check
					let (start_row, rows) = if row == 0 {
						(0, 2)
					} else if row == grid.len() - 1 {
						(row - 1, 2)
					} else {
						(row - 1, 3)
					};

					let (start_col, cols) = if col == 0 {
						(0, 2)
					} else if col == line.len() - 1 {
						(col - 1, 2)
					} else {
						(col - 1, 3)
					};

					let mut nums = Vec::new();

					// Check the cells
					for row in grid.iter().skip(start_row).take(rows) {
						for cell in row.iter().skip(start_col).take(cols) {
							if let GridCell::Number(n) = &cell {
								nums.push(*n.borrow());
							}
						}
					}

					// Assumes numbers are unique
					nums.sort_unstable();
					nums.dedup();

					if nums.len() == 2 {
						parts_total += nums[0] * nums[1];
					}
				}
			}
		}
	}

	println!("{parts_total}");
}
